# @groovox/serverless

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines]

It contains serverless functions that may be useful for Groovox.

## Functions

### [transform-image](./function/transform-image)

Transforms images from S3 bucket runtime and secure by JSON web token.

[license]: https://gitlab.com/groovox/serverless/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green
[pipelines]: https://gitlab.com/groovox/serverless/pipelines
[pipelines_badge]: https://gitlab.com/groovox/serverless/badges/master/pipeline.svg
