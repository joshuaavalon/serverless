import { Client } from "minio";

import { readConfig } from "./config";

export const createClient = (): Client => {
  const { s3 } = readConfig();
  const { bucket, ...others } = s3;
  return new Client(others);
};
