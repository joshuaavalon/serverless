import { APIGatewayProxyHandlerV2 } from "aws-lambda";

import { createClient } from "./storage";
import { readConfig } from "./config";
import { getQuery, responseError, responseJson } from "./utils";
import { validateJwt } from "./jwt";

export const handler: APIGatewayProxyHandlerV2 = async event => {
  try {
    const q = getQuery(event);
    const query = validateJwt(q);
    const { key = "", maxSize, expire = 60 * 60 } = query;
    const { bucket, sessionToken } = readConfig().s3;
    const client = createClient();
    const policy = client.newPostPolicy();
    policy.setBucket(bucket);
    const expires = new Date();
    expires.setSeconds(expire);
    policy.setExpires(expires);
    policy.setKey(key);
    if (maxSize && maxSize > 0) {
      policy.setContentLengthRange(0, maxSize * 1024 * 1024);
    }
    const signed = await client.presignedPostPolicy(policy);
    // https://github.com/minio/minio-js/issues/898
    if (!("x-amz-security-token" in signed.formData) && sessionToken) {
      signed.formData["x-amz-security-token"] = sessionToken;
    }
    return responseJson(signed as any);
  } catch (e) {
    return responseError(e);
  }
};
