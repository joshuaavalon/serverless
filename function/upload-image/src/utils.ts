import {
  APIGatewayProxyEventV2,
  APIGatewayProxyStructuredResultV2
} from "aws-lambda";
import { AppError } from "./error";

export const responseJson = (
  body: Record<string, unknown>,
  statusCode = 200
): APIGatewayProxyStructuredResultV2 => ({
  statusCode,
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify(body)
});

export const responseJsonError = (
  error: string,
  statusCode = 400
): APIGatewayProxyStructuredResultV2 => responseJson({ error }, statusCode);

export const responseError = (
  error: unknown
): APIGatewayProxyStructuredResultV2 => {
  if (error instanceof AppError) {
    return responseJsonError(error.message, error.statusCode);
  }
  const message = error instanceof Error ? error.message : "Unknown error(s)";
  return responseJsonError(message);
};

export const getQuery = (event: APIGatewayProxyEventV2): string => {
  const { body } = event;
  if (!body) {
    throw new AppError("Missing query parameter(s)");
  }
  return body;
};
