import { APIGatewayProxyHandlerV2 } from "aws-lambda";

import { transformImage } from "./transform";
import { createClient } from "./storage";
import { readConfig } from "./config";
import { getQuery, responseBase64, responseError } from "./utils";
import { validateJwt } from "./jwt";
import { loadStat } from "./s3";

export const handler: APIGatewayProxyHandlerV2 = async event => {
  try {
    const q = getQuery(event);
    const query = validateJwt(q);
    const { key = "", image = [] } = query;
    const { bucket } = readConfig().s3;
    const client = createClient();
    const stat = await loadStat(client, bucket, key);
    const rs = await client.getObject(bucket, key);
    const { body, contentType } = await transformImage(rs, image);
    return responseBase64(body, contentType, stat);
  } catch (e) {
    return responseError(e);
  }
};
