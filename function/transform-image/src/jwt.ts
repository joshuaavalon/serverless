import jwt from "jsonwebtoken";

import { AppError } from "./error";
import type { Query } from "./type";

export const validateJwt = (token: string): Query => {
  const { JWT_SECRET: secret = "" } = process.env;
  try {
    return jwt.verify(token, secret) as any;
  } catch (e) {
    throw new AppError("Invalid JWT", 403);
  }
};
