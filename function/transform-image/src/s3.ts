import { BucketItemStat, Client } from "minio";
import { AppError } from "./error";

export const loadStat = async (
  client: Client,
  bucket: string,
  key: string
): Promise<BucketItemStat> => {
  try {
    return await client.statObject(bucket, key);
  } catch (e) {
    if (e.message) {
      console.error(e.message);
    }
    throw new AppError("Invalid bucket or key", 404);
  }
};
